<?php
class DB_Connect {
    private $conn;

    // Connecting to database
    public function connect() {
        require_once 'cnx.php';

        // Connecting to mysql database
        $this->conn = new PDO(Dsn, DB_USER, DB_PASSWORD,OPT);

        // return database handler
        return $this->conn;
    }
}

?>