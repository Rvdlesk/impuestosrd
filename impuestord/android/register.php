
<?php

require_once 'DB_Functions.php';
$db = new DB_Functions();

// json response array
$response = array("error" => FALSE);

if(isset($_POST['re'])) {

    if (isset($_POST['cedula'])) {

        $cedula = $_POST['cedula'];

        // check if user is already existed with the same cedula
        if ($db->isUserExisted($cedula)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "Usuario ya existe " . $cedula;
            echo json_encode($response);

        } else {
            // create a new user
            $user = $db->verifyCedula($cedula);
            if ($user) {
                // user stored successfully
                $response["error"] = FALSE;
                $response["user"]["name"] = $user["nombres"];
                $response["user"]["last1"] = $user["apellido1"];
                $response["user"]["last2"] = $user["apellido2"];
                $response["user"]["cedula"] = $user["cedula"];
                $response["user"]["FechaNacimiento"] = $user["FechaNacimiento"];
                echo json_encode($response);
            } else {
                // user failed to store
                $response["error"] = TRUE;
                $response["error_msg"] = "Unknown error occurred verify cedula!";
                echo json_encode($response);
            }
        }
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Required parameters (cedula) is missing!";
        echo json_encode($response);

    }
}else {

    if (isset($_POST['name']) && isset($_POST['last']) && isset($_POST['ced']) && isset($_POST['email']) && isset($_POST['user']) && isset($_POST['password']) && isset($_POST['docType']) && isset($_POST['bday'])) {

        // receiving the post params
        $name = $_POST['name'];
        $last = $_POST['last'];
        $ced = $_POST['ced'];
        $email = $_POST['email'];
        $user = $_POST['user'];
        $password = $_POST['password'];
        $dt = $_POST['docType'];
        $bday = $_POST['bday'];


        // check if user is already existed with the same email
        if ($db->isUserExisted($ced)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "User already existed with " . $ced;
            echo json_encode($response);
        } else {
            // create a new user
            $user = $db->storeUser($name, $last, $ced, $email, $user, $password, $dt, $bday);
            if ($user) {
                // user stored successfully
                $response["error"] = FALSE;
//            $response["uid"] = $user["unique_id"];
//            $response["user"]["name"] = $user["name"];
//            $response["user"]["email"] = $user["email"];
//            $response["user"]["created_at"] = $user["created_at"];
//            $response["user"]["updated_at"] = $user["updated_at"];
                echo json_encode($response);
            } else {
                // user failed to store
                $response["error"] = TRUE;
                $response["error_msg"] = "Unknown error occurred in registration!";
                echo json_encode($response);
            }
        }
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Required parameters (name, email or password) is missing!";
        echo json_encode($response);
    }
}

?>