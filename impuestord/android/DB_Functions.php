<?php


class DB_Functions {

    private $conn;

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {

    }

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name,$last,$ced, $email,$user, $password,$dt,$bday) {
        $uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"];
        $date = date('Y-m-d H:i:s');

        $rows = [
            'DType' => $dt,
            'DocNumber' => $ced,
            'Names' => $name,
            'LastNames' => $last,
            'BDay' => $bday

        ];

        $sql = "INSERT INTO Users SET DType=:DType, DocNumber=:DocNumber, Names=:Names, LastNames=:LastNames, BDay=:BDay;";

        $stmt = $this->conn->prepare($sql)->execute($rows);

        // check for successful store
        if ($stmt) {

            $lastId = $this->conn->lastInsertId();
            $rows2 = [
                'UserName' => $user,
                'UniqueId' => $uuid,
                'Pwd' => $encrypted_password,
                'Pwd2' => $salt,
                'CreateAt' => $date,
                'Email' => $email,
                'UserId' => $lastId

            ];
            $sql2 = "INSERT INTO UserLogin SET UserName=:UserName, UniqueId=:UniqueId, Pwd=:Pwd, Pwd2=:Pwd2, CreateAt=:CreateAt, Email=:Email, UserId=:UserId;";
            $stmt2 = $this->conn->prepare($sql2)->execute($rows2);

            if ($stmt2) {
                return true;
            } else {
                return false;
            }
        }else {return false;}
    }

    public function verifyCedula($cedula){
        $stmt = $this->conn->prepare("SELECT cedula,nombres,apellido1,apellido2,FechaNacimiento FROM padron WHERE cedula = :cedula limit 1");
        $stmt->execute(['cedula' => $cedula]);
        $user = $stmt->fetch();


        return $user;

    }

    /**
     * Get user by email and password
     */
    public function getUserByEmailAndPassword($user, $password) {

        $stmt = $this->conn->prepare("SELECT * FROM UserLogin WHERE Username =:UserName");

        $stmt->execute(['UserName' => $user]);


        if ($stmt) {
            $user = $stmt->fetch();

            // verifying user password
            $salt = $user['Pwd2'];
            $encrypted_password = $user['Pwd'];
            $hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return $user;
            }
        } else {
            return NULL;
        }
    }

    /**
     * Check user is existed or not
     */
    public function isUserExisted($cedula) {
        $stmt = $this->conn->prepare("SELECT DocNumber from Users WHERE DocNumber=:cedula");

        $stmt->execute(['cedula' => $cedula]);


        $stmt->fetch();

        if ($stmt->rowCount() > 0) {
            // user existed

            return true;
        } else {
            // user not existed

            return false;
        }
    }

    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }

    public function getRnc (){
        $stmt = $this->conn->prepare("SELECT RNC,Nombre,status FROM empresas");
        $stmt->execute();
        $user = $stmt->fetchAll();


        return $user;
    }

    public function getRegisterNcf (){
        $stmt = $this->conn->prepare("SELECT r.Rnc,r.Ncf,r.Fecha,r.Monto,r.Itbis,r.tipo,e.Nombre FROM registro_ncf r INNER JOIN empresas e on r.empresa_id= e.empresa_id");
        $stmt->execute();
        $user = $stmt->fetchAll();


        return $user;
    }



    public function storeNcf($user_id,$select,$select2,$ncf,$monto, $fecha, $rnc,$imagen){


     $stmt = $this->conn->prepare("SELECT * FROM empresas where RNC=:rnc");
        $stmt->execute(['rnc' => $rnc]);
        $e = $stmt->fetch();



        $rows = [
            'user_id' => $user_id,
            'tipo' => $select,
            'select2' => $select2,
            'ncf' => $ncf,
            'monto' => $monto,
//            'itbis' => $itbis,
            'fecha' => $fecha,
            'rnc' => $rnc,
            'empresa_id' =>$e['empresa_id'],
            'Imagen'=> $imagen

        ];

        $sql = "INSERT INTO registro_ncf SET user_id=:user_id, Ncf=:ncf, Monto=:monto,  Fecha=:fecha, Rnc=:rnc, empresa_id=:empresa_id , Imagen=:Imagen, tipoNcf=:tipo, tipoServicio=:select2;";

        $stmt = $this->conn->prepare($sql)->execute($rows);

        return $stmt;


    }

    public function storeDenuncia($select,$ncf, $rnc, $tel, $dir, $comen, $imagen,$user){



        $date = date('Y-m-d H:i:s');


        $rows = [

            'opti' =>   $select,
            'ncf' =>    $ncf,
            'rnc' =>    $rnc,
            'tel' =>    $tel,
            'dir' =>    $dir,
            'comen'=>   $comen,
            'userid'=>  $user,
            'dates'=>   $date,
            'imagen'=>  $imagen,


        ];

        $sql = "INSERT INTO denuncia SET TipoDenuncia=:opti, RNC=:rnc, Ncf=:ncf, Direccion=:dir, Telefono=:tel, Comentario=:comen, UserId=:userid, Dates=:dates, Images=:imagen;";

        $stmt = $this->conn->prepare($sql);
            if($stmt->execute($rows)){
            print("se hizo");
            }else{
                echo "nc";
            }
exit;
        return $stmt;


    }
    public function getDash($user){
        $stmt = $this->conn->prepare("SELECT tipoServicio as Servicio,count(tipoServicio) as count from registro_ncf  where user_id=:user_id GROUP by tipoServicio");
        $stmt->execute(['user_id' => $user]);
        $data = $stmt->fetchAll();

        Return $data;
    }

}

?>