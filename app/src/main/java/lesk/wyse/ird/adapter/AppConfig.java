package lesk.wyse.ird.adapter;

/**
 * Created by clevo on 11/18/2017.
 */

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://impuestord.com/android/login.php";

    // Server user register url
    public static String URL_REGISTER = "http://impuestord.com/android/register.php";

    // Server store auto complete
    public static String URL_REGISTRO_NCF = "http://impuestord.com/android/registroncf.php";


    public static String URL_DENUNCIA = "http://impuestord.com/android/denuncia.php";

    public static String URL_GASTO_REPORT = "http://impuestord.com/android/gasto_report.php";

    public static String URL_DASHBOARD = "http://impuestord.com/android/get_dash.php";
}
