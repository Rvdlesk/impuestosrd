package lesk.wyse.ird.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lesk.wyse.ird.R;
import lesk.wyse.ird.model.RegisterNcf;

/**
 * Created by clevo on 2/25/2018.
 */

public class GastoDataAdapter extends RecyclerView.Adapter<GastoDataAdapter.ViewHolder> {
    private Context context;
    private List<RegisterNcf> registerNcf;

    public GastoDataAdapter(Context context, List registerNcf) {
        this.context = context;
        this.registerNcf = registerNcf;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_report, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(registerNcf.get(position));

        RegisterNcf pu = registerNcf.get(position);

        holder.Ncf.setText(pu.getNcf());
        holder.Monto.setText(pu.getMonto());
        holder.Nombre.setText(pu.getNombre());

    }

    @Override
    public int getItemCount() {
        return registerNcf.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView Ncf;
        public TextView Monto;
        public TextView Itbis;
        public TextView Fecha;
        public TextView Rnc;
        public TextView Tipo;
        public TextView Nombre;


        public ViewHolder(View itemView) {
            super(itemView);

            Ncf = (TextView) itemView.findViewById(R.id.Ncf);
            Monto = (TextView) itemView.findViewById(R.id.Monto);
            Itbis = (TextView) itemView.findViewById(R.id.Itbis);
            Fecha = (TextView) itemView.findViewById(R.id.Fecha);
            Rnc = (TextView) itemView.findViewById(R.id.Rnc);
            Tipo = (TextView) itemView.findViewById(R.id.Tipo);
            Nombre = (TextView) itemView.findViewById(R.id.Nombre);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    RegisterNcf cpu = (RegisterNcf) view.getTag();

                    Toast.makeText(view.getContext(), cpu.getNcf()+" "+cpu.getMonto(), Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

}
