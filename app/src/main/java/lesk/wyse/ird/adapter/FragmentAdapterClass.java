package lesk.wyse.ird.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import lesk.wyse.ird.Tab_1_Activity;
import lesk.wyse.ird.Tab_2_Activity;

/**
 * Created by clevo on 11/17/2017.
 */

public class FragmentAdapterClass extends FragmentStatePagerAdapter {

    int TabCount;

    public FragmentAdapterClass(FragmentManager fragmentManager, int CountTabs) {

        super(fragmentManager);

        this.TabCount = CountTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Tab_1_Activity tab1 = new Tab_1_Activity();
                return tab1;

            case 1:
                Tab_2_Activity tab2 = new Tab_2_Activity();
                return tab2;



            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return TabCount;
    }
}

