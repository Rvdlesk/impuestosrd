package lesk.wyse.ird;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import lesk.wyse.ird.adapter.AppConfig;
import lesk.wyse.ird.adapter.AppController;
import lesk.wyse.ird.adapter.SessionManager;
import lesk.wyse.ird.adapter.Utility;
import lesk.wyse.ird.model.SQLiteHandler;

import static android.util.Log.d;
import static android.util.Log.e;


public class RegistroNcfActivity extends BaseActivity {
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button btnSelect,btnDenuncia,btnFecha;
    private ImageButton send;
    private EditText ncf,monto,itbis;
    private TextView alert1,sta;
    private ImageView ivImage;
    private String userChoosenTask,imageUrl;
    private MaterialSpinner spinner,spinner2;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private String status,ncf1;
    private boolean status2;
//    private static final String TAG = "Sample";
    NavigationView navigationView;
    private static final String TAG = RegisterActivity.class.getSimpleName();

    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";

    private static final String STATE_TEXTVIEW = "STATE_TEXTVIEW";
    private EditText dates;

    private SwitchDateTimeDialogFragment dateTimeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_registro_ncf, frameLayout);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        spinner2 = (MaterialSpinner) findViewById(R.id.spinner2);

        String[] letra2 = {"Tipo de Servicio","Restaurantes","Salud","Servicios","Super Mercado","Tiendas","Otros"};
        spinner2.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, letra2));

        String[] letra = {"Tipo de NCF","Consumidor Final","Impuestos Gubernamentales","Impuestos por Transferencias","Gastos Educativos"};
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, letra));

        btnSelect = (Button) findViewById(R.id.cpic);
        btnSelect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        btnFecha = (Button) findViewById(R.id.button2);
        btnDenuncia = (Button) findViewById(R.id.denun);
        alert1 = (TextView) findViewById(R.id.alert1);
        sta = (TextView) findViewById(R.id.status);
        monto = (EditText) findViewById(R.id.input_monto);
        itbis = (EditText) findViewById(R.id.input_itbis);
        send = (ImageButton) findViewById(R.id.send);
        final AutoCompleteTextView input_rnc = (AutoCompleteTextView) findViewById(R.id.input_rnc);
        ncf = (EditText) findViewById(R.id.input_ncf);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(item.equals("Consumidor Final") || item.equals("Impuestos por Transferencias") || item.equals("Gastos Educativos")){
                   ncf1 = "B01 - ";
                    ncf.setText(ncf1);
                }else if(item.equals("Impuestos Gubernamentales")){
                    ncf1 ="B15 - ";
                    ncf.setText(ncf1);
                }

            }
        });

        Selection.setSelection(ncf.getText(), ncf.getText().length());
        ncf.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }



            @Override
            public void afterTextChanged(Editable s) {

                if (!s.toString().startsWith(ncf1)) {
                    ncf.setText(ncf1);
                    Selection.setSelection(ncf.getText(), ncf.getText().length());

                }

            }
        });


        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        ivImage = (ImageView) findViewById(R.id.Imageprev);
        dates = (EditText) findViewById(R.id.input_date);

        if (savedInstanceState != null) {
            // Restore value from saved state
            dates.setText(savedInstanceState.getCharSequence(STATE_TEXTVIEW));
        }

// Construct SwitchDateTimePicker
        dateTimeFragment = (SwitchDateTimeDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
//                    getString(R.string.clean) // Optional
            );
        }


        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("d MMM yyyy HH:mm", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(2015, Calendar.JANUARY, 1).getTime());
        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2050, Calendar.DECEMBER, 31).getTime());

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            e(TAG, e.getMessage());
        }

        // Set listener for date
        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                dates.setText(myDateFormat.format(date));
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
                dates.setText("");
            }
        });

        Button buttonView = (Button) findViewById(R.id.button2);
        buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Re-init each time
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar().getTime());
                dateTimeFragment.show(getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);
            }
        });
        getRnc();
        Button buttomSend = (Button) findViewById(R.id.denun);
        buttomSend.setOnClickListener (new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(RegistroNcfActivity.this,DenunciaActivity.class);
                startActivity(myIntent);
            }
        });

        // Register Button Click event
       send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String select = spinner.getText().toString();
                String select2 = spinner2.getText().toString();
                String ncf2 = ncf.getText().toString().trim().replace("-", "").replaceAll("\\s+","");
                String rnc = input_rnc.getText().toString().trim().substring(0,9);
                String montos = monto.getText().toString().trim();
//                String itbis2 = itbis.getText().toString().trim();
                String images = imageUrl;
                String date = dates.getText().toString().trim();
                registerNcf(select,select2,ncf2,montos, images, date,rnc);
//                boolean chk =checkBox.isChecked();

//                if(chk){
//                    if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty() && !last.isEmpty() && !rpassword.isEmpty() && !cedula.isEmpty()) {
//                        if (password.equals(rpassword)){
//
//                            registerUser(name,last,cedula, email, user,password,docType, bday);
//                        }else{Toast.makeText(getApplicationContext(),
//                                "La contraseña no coicide", Toast.LENGTH_LONG)
//                                .show();}
//                    } else {
//                        Toast.makeText(getApplicationContext(),
//                                "Complete los campos", Toast.LENGTH_LONG)
//                                .show();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(),
//                            "Debe aceptar los terminos", Toast.LENGTH_LONG)
//                            .show();
//                }
            }
        });



    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // to check current activity in the navigation drawer
        navigationView.getMenu().getItem(2).setChecked(true);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the current textView
        savedInstanceState.putCharSequence(STATE_TEXTVIEW, dates.getText());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Tomar Foto"))
                        cameraIntent();
//                    else if(userChoosenTask.equals("Elegir de la galeria"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }
    private void selectImage() {
        final CharSequence[] items = { "Tomar Foto",
                "Cancelar" };

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistroNcfActivity.this);
        builder.setTitle("Agregar Foto!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(RegistroNcfActivity.this);

                if (items[item].equals("Tomar Foto")) {
                    userChoosenTask ="Tomar Foto";
                    if(result)
                        cameraIntent();

//                } else if (items[item].equals("Elegir de la galeria")) {
//                    userChoosenTask ="Elegir de la galeria";
//                    if(result)
//                        galleryIntent();
//
//                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        Bitmap photo = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bArray = bos.toByteArray();
        imageUrl= String.valueOf(bArray);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivImage.setImageBitmap(bm);
    }


    private void getRnc() {
        // Tag used to cancel the request
        String tag_string_req = "req_rnc";

//        pDialog.setMessage("Espere");
//        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTRO_NCF, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                d(TAG, "Login Response: " + response.toString());
//                hideDialog();

                try {
                    JSONArray jObj = new JSONArray(response);
//                    boolean error = jObj.getBoolean("error");
//
//                    // Check for error node in json
//                    if (!error) {
                        final String[] str1 = new String[jObj.length()];
                       final String[] str2 = new String[jObj.length()];
                        JSONObject json= null;

                        for(int i=0;i<jObj.length();i++)
                        {

                            json=jObj.getJSONObject(i);
                            str1[i]= json.getString("RNC") + " - " + json.getString("Nombre");
                            str2[i] =json.getString("status");
                        }

                        final AutoCompleteTextView text = (AutoCompleteTextView) findViewById(R.id.input_rnc);
                        final List<String> list = new ArrayList<String>();

                        for(int i=0;i<str1.length;i++)
                        {
                            list.add(str1[i]);
                        }

                        Collections.sort(list);

                        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                                (getApplicationContext(), android.R.layout.simple_dropdown_item_1line, list);

                        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        text.setThreshold(3);
                        text.setAdapter(dataAdapter);

                        text.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                                // TODO Auto-generated method stub
                                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                                in.hideSoftInputFromWindow(arg1.getApplicationWindowToken(), 0);
                                int pos = -1;
                                String selection = (String) parent.getItemAtPosition(position);
                                for (int i = 0; i < str1.length; i++) {
                                    if (str1[i].equals(selection)) {
                                        pos = i;
                                        break;
                                    }
                                }


                                for(int i=0;i<str2.length;i++) {
                                    if (str2[pos].equals("0")) {
                                        sta.setText(" Empresa Inactiva");
                                        sta.setTextColor(Color.parseColor("#F44336"));
                                    }else{
                                        sta.setText("Empresa Activa");
                                        sta.setTextColor(Color.parseColor("#00E676"));
                                    }
                                System.out.println("Position "  + str2[i]);

                                }
                                hideKeyboard(arg1);
                                sta.setVisibility(View.VISIBLE);
                                btnDenuncia.setVisibility(View.GONE);
                                alert1.setVisibility(View.GONE);
                                ncf.setVisibility(View.VISIBLE);
                                monto.setVisibility(View.VISIBLE);
//                                itbis.setVisibility(View.VISIBLE);
                                btnSelect.setVisibility(View.VISIBLE);
                                dates.setVisibility(View.VISIBLE);
                                btnFecha.setVisibility(View.VISIBLE);
                                spinner.setVisibility(View.VISIBLE);
                                spinner2.setVisibility(View.VISIBLE);
                                send.setVisibility(View.VISIBLE);

                            }
                        });

                    text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            // When textview lost focus check the textview data valid or not
                            if (!hasFocus) {
                                if (!list.contains(text.getText().toString())) {
                                    text.setText(""); // clear your TextView
                                    d(TAG,"editetxt: " );
                                    btnDenuncia.setVisibility(View.GONE);
                                    alert1.setVisibility(View.GONE);
                                }
                            }
                        }
                        });


                    text.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before,
                                                  int count) {

                                if (text.length() < 4){
                                    btnDenuncia.setVisibility(View.GONE);
                                    alert1.setVisibility(View.GONE);

                            }
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                      int after) {
                            if (!list.contains(text.getText().toString())) {
                                if(count>4){
                                    btnDenuncia.setVisibility(View.VISIBLE);
                                    alert1.setVisibility(View.VISIBLE);
                                    ncf.setVisibility(View.GONE);
                                    monto.setVisibility(View.GONE);
//                                    itbis.setVisibility(View.GONE);
                                    btnSelect.setVisibility(View.GONE);
                                    dates.setVisibility(View.GONE);
                                    btnFecha.setVisibility(View.GONE);
                                    send.setVisibility(View.GONE);
                                    sta.setVisibility(View.GONE);
                                    spinner2.setVisibility(View.GONE);
                                    send.setVisibility(View.GONE);


                                }
                            }

                        }

                        @Override
                        public void afterTextChanged(Editable s) {


                        }

                    });
//                    } else {
//                        // Error in login. Get the error message
//                        String errorMsg = jObj.getString("error_msg");
//                        Toast.makeText(getApplicationContext(),
//                                errorMsg, Toast.LENGTH_LONG).show();
//                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    d(TAG,"Json error: " + e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("rn", "1");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void registerNcf(final String select,final String select2, final String ncf, final String montos,final String
                                                                images,final String date, final String rnc) {
        // Tag used to cancel the request
        String tag_string_req = "req_register_ncf";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTRO_NCF, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
//                        // User successfully stored in MySQL
//                        // Now store the user in sqlite
//                        String uid = jObj.getString("uid");
//
//                        JSONObject user = jObj.getJSONObject("user");
//                        String name = user.getString("name");
//                        String email = user.getString("email");
//                        String created_at = user
//                                .getString("created_at");
//
//                        // Inserting row in users table
//                        db.addUser(name, email, uid, created_at);

                        Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();
                        // Launch login activity
                        Intent intent = new Intent(
                                RegistroNcfActivity.this,
                                LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
//                        String errorMsg = jObj.getString("error_msg");
//                        Toast.makeText(getApplicationContext(),
//                                errorMsg, Toast.LENGTH_LONG).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegistroNcfActivity.this);
                        builder.setMessage("Factura Registrada Satisfactoriamente")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent i=new Intent(RegistroNcfActivity.this, RegistroNcfActivity.class);
                                        i.setFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                        finish();
                                        startActivity(i);
                                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "1");
                params.put("select", select);
                params.put("select2", select2);
                params.put("Ncf", ncf);
                params.put("Monto", montos);
//                params.put("Itbis", itbis2);
                params.put("Fecha", date);
                params.put("Rnc", rnc);
                params.put("Imagen", images);
                params.put("rg", "1");



                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}





