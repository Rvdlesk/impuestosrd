package lesk.wyse.ird;

import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Calendar extends AppCompatActivity {
    List<String> li;
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);


        li=new ArrayList<String>();
        li.add("Pago de energia eléctrica");
        li.add("Pago de agua");
        li.add("Pago de teléfono");
        li.add("Pago de Préstamo");
        li.add("Pago de Tarjeta de credito");
        li.add("Pago de servicios domesticos");
        li.add("Pago de mantenimiento");
        li.add("Pago de la basura");
        li.add("Otros");



        list=(ListView) findViewById(R.id.listView1);



        ArrayAdapter<String> adp=new ArrayAdapter<String>
                (getBaseContext(),R.layout.list,li);
        list.setAdapter(adp);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                Toast.makeText(getBaseContext(), li.get(arg2),
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType("vnd.android.cursor.item/event");

                java.util.Calendar cal = java.util.Calendar.getInstance();
                long startTime = cal.getTimeInMillis();
                long endTime = cal.getTimeInMillis()  + 60 * 60 * 1000;

                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime);
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,endTime);
                intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
if(!Objects.equals(li.get(arg2), "Otros")){
    intent.putExtra(CalendarContract.Events.TITLE, li.get(arg2));
}
                intent.putExtra(CalendarContract.Events.DESCRIPTION,  "Recuerda que debes pagar este servicio");
                intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "No aplica");
                intent.putExtra(CalendarContract.Events.RRULE, "FREQ=MONTHLY");

                startActivity(intent);
            }
        });
    }



}
