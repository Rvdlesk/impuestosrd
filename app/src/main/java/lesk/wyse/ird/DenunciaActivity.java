package lesk.wyse.ird;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lesk.wyse.ird.adapter.AppConfig;
import lesk.wyse.ird.adapter.AppController;
import lesk.wyse.ird.adapter.SessionManager;
import lesk.wyse.ird.adapter.Utility;
import lesk.wyse.ird.model.SQLiteHandler;

/**
 * Created by clevo on 12/17/2017.
 */

public class DenunciaActivity extends BaseActivity {

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button btnSelect,btnDenuncia,btnFecha,send;
    private EditText ncf,tel,dir,rnc,comen;
    private TextView alert1;
    private ImageView ivImage;
    private String userChoosenTask;
    private Spinner spinner;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private static final String TAG = "Sample";
    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_denuncia, frameLayout);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        spinner = (Spinner) findViewById(R.id.spinner);
        String[] letra = {"Factura sin numero de comprobante fiscal","Facturas sin itbis","Negocio sin factura"};
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, letra));
        ivImage = (ImageView) findViewById(R.id.Imageprev);
        btnSelect = (Button) findViewById(R.id.cpic);
        rnc = (EditText) findViewById(R.id.input_rnc);
        ncf = (EditText) findViewById(R.id.input_ncf);
        tel = (EditText) findViewById(R.id.input_tel);
        dir = (EditText) findViewById(R.id.input_dir);
        comen = (EditText) findViewById(R.id.input_comen);
        send = (Button) findViewById(R.id.send);

        btnSelect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
//        dates = (EditText) findViewById(R.id.input_date);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Register Button Click event
        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String select = spinner.getSelectedItem().toString();
                String ncf2 = ncf.getText().toString().trim();
                String rnc2 = rnc.getText().toString().trim();
                String tel2 = tel.getText().toString().trim();
                String dir2 = dir.getText().toString().trim();
                String comen2 = comen.getText().toString().trim();
                String images = String.valueOf(ivImage);
                String user = "1";


                denunciarNcf(select,ncf2,rnc2,tel2,dir2,comen2,images,user);
//                boolean chk =checkBox.isChecked();

//                if(chk){
//                    if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty() && !last.isEmpty() && !rpassword.isEmpty() && !cedula.isEmpty()) {
//                        if (password.equals(rpassword)){
//
//                            registerUser(name,last,cedula, email, user,password,docType, bday);
//                        }else{Toast.makeText(getApplicationContext(),
//                                "La contraseña no coicide", Toast.LENGTH_LONG)
//                                .show();}
//                    } else {
//                        Toast.makeText(getApplicationContext(),
//                                "Complete los campos", Toast.LENGTH_LONG)
//                                .show();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(),
//                            "Debe aceptar los terminos", Toast.LENGTH_LONG)
//                            .show();
//                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        // to check current activity in the navigation drawer
        navigationView.getMenu().getItem(3).setChecked(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(DenunciaActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(DenunciaActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ivImage.setImageBitmap(bm);
    }




    private void denunciarNcf(final String select ,final String ncf, final String rnc,final String tel,final String
            dir,final String comen, final String image,final String user) {
        // Tag used to cancel the request
        String tag_string_req = "req_denuncia";

        pDialog.setMessage("Denunciando...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DENUNCIA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
//                        // User successfully stored in MySQL
//                        // Now store the user in sqlite
//                        String uid = jObj.getString("uid");
//
//                        JSONObject user = jObj.getJSONObject("user");
//                        String name = user.getString("name");
//                        String email = user.getString("email");
//                        String created_at = user
//                                .getString("created_at");
//
//                        // Inserting row in users table
//                        db.addUser(name, email, uid, created_at);

                        Toast.makeText(getApplicationContext(), "User successfully denunciado. Try login now!", Toast.LENGTH_LONG).show();
                        // Launch login activity
//                        Intent intent = new Intent(
//                                DenunciaActivity.this,
//                                LoginActivity.class);
//                        startActivity(intent);
//                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("Select", select);
                params.put("Ncf", ncf);
                params.put("Rnc", rnc);
                params.put("Tel", tel);
                params.put("Dir", dir);
                params.put("Comen", comen);
                params.put("Imagen", image);
                params.put("User", user);
                params.put("de", "1");
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
