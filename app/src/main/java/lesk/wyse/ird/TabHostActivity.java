package lesk.wyse.ird;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import lesk.wyse.ird.adapter.AppConfig;
import lesk.wyse.ird.adapter.AppController;
import lesk.wyse.ird.adapter.FragmentAdapterClass;
import lesk.wyse.ird.adapter.SessionManager;
import lesk.wyse.ird.model.SQLiteHandler;

/**
 * Created by clevo on 11/17/2017.
 */

public class TabHostActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    Toolbar toolbar;
    private Fragment btnRegister;
    private EditText input_cedula;
    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentAdapterClass fragmentAdapter;
    public ProgressDialog pDialog;
    public SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tab_host);

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout1);
        viewPager = (ViewPager) findViewById(R.id.pager1);
        setSupportActionBar(toolbar);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());


        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // it's getSupportActionBar() if you're using AppCompatActivity, not getActionBar()

        tabLayout.addTab(tabLayout.newTab().setText("Cedula"));
        tabLayout.addTab(tabLayout.newTab().setText("Pasaporte"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        fragmentAdapter = new FragmentAdapterClass(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(fragmentAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab LayoutTab) {

                viewPager.setCurrentItem(LayoutTab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab LayoutTab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab LayoutTab) {

            }
        });




    }

    public void getCedula(String cedula){

                if (!cedula.isEmpty() && cedula.length()==11) {
        checkCedula(cedula);
    } else {
        // Prompt user to enter credentials
        Toast.makeText(getApplicationContext(),
                "Please enter the credentials!", Toast.LENGTH_LONG)
                .show();
    }
}

    public void checkCedula(final String cedula) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Verificando ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session

                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        String last = user.getString("last1") + " " + user.getString("last2");
                        String cedula = user.getString("cedula");
                        String bday = user.getString("FechaNacimiento");


                        // Launch main activity
                        Intent intent = new Intent(TabHostActivity.this, RegisterActivity.class);
                        intent.putExtra("names", name);
                        intent.putExtra("last", last);
                        intent.putExtra("cedula", cedula);
                        intent.putExtra("bday", bday);

                        startActivity(intent);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "register Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("cedula", cedula);
                params.put("re", "1");


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: //hago un case por si en un futuro agrego mas opciones
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
