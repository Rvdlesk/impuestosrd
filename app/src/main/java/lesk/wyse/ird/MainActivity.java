package lesk.wyse.ird;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lesk.wyse.ird.adapter.AppConfig;
import lesk.wyse.ird.adapter.AppController;
import lesk.wyse.ird.adapter.SessionManager;
import lesk.wyse.ird.model.SQLiteHandler;

import static java.sql.Types.NULL;

public class MainActivity extends BaseActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    NavigationView navigationView;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    int ot ,sm,sc,rt,td,sl ;

    PieChart pieChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_main, frameLayout);

        GetDash();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        String tag_string_req = "req_dash";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DASHBOARD, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());
//                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");


                    if (!error) {
                        // user successfully logged in
                        // Create login session
//                        session.setLogin(true);

//                        // Now store the user in SQLite
//                        String uid = jObj.getString("uid");
//
                        JSONObject user = jObj.getJSONObject("user");
                        ot = user.has("Ot") ? Integer.parseInt(user.getString("Ot")) : 0;
                        sm = user.has("sm") ? Integer.parseInt(user.getString("sm")) : 0;
                        sl = user.has("sl") ? Integer.parseInt(user.getString("sl")) : 0;
                        sc = user.has("sc") ? Integer.parseInt(user.getString("sc")) : 0;
                        td = user.has("td") ? Integer.parseInt(user.getString("td")) : 0;
                        rt = user.has("rt") ? Integer.parseInt(user.getString("rt")) : 0;


                        Log.d(TAG, "Response: " + ot);
//                        String email = user.getString("email");
//                        String created_at = user.getString("created_at");
//
//                        // Inserting row in users table
//                        db.addUser(name, email, uid, created_at);

                        pieChart = (PieChart) findViewById(R.id.chart1);
                        pieChart.setUsePercentValues(false);
                        int ots[] = {ot,sl,sc,sm,td,rt};

//

                        final ArrayList<Entry> yvalues = new ArrayList<Entry>();

                        for (int i = 0; i < ots.length; i++) {
                            if(ots[i] == 0){
                                continue;
                            }
                            yvalues.add(new Entry(ots[i], i));
                        }
                        ValueFormatter formatter = new ValueFormatter() {
                            @Override
                            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                                return ((int) value) + " " ;
                            }
                        };

                        PieDataSet dataSet = new PieDataSet(yvalues, "Impuestos");

                        ArrayList<String> xVals = new ArrayList<String>();
                        if (user.has("Ot")) xVals.add( "Otros");
                        if (user.has("sl")) xVals.add( "Salud");
                        if (user.has("sc")) xVals.add( "Servicios");
                        if (user.has("sm")) xVals.add( "Super mercado");
                        if (user.has("td")) xVals.add("Tiendas");
                        if (user.has("rt")) xVals.add("Restaurantes");


                        PieData data = new PieData(xVals, dataSet);

                        data.setValueFormatter(formatter);
                        // In percentage Term
//                        data.setValueFormatter(new PercentFormatter());
                        pieChart.setUsePercentValues(false);
// Default value
//data.setValueFormatter(new DefaultValueFormatter(0));
                        pieChart.setData(data);
                        pieChart.setDrawHoleEnabled(true);
                        pieChart.setDescription("");
                        pieChart.getLegend().setEnabled(false);
                        data.setValueTextSize(11f);
                        data.setValueTextColor(Color.DKGRAY);
                        pieChart.animateXY(1400, 1400);

//                        final int[] MY_COLORS = {Color.rgb(192,0,0), Color.rgb(255,0,0), Color.rgb(255,192,0),
//                                Color.rgb(127,127,127), Color.rgb(146,208,80), Color.rgb(0,176,80), Color.rgb(79,129,189)};
//                        ArrayList<Integer> colors = new ArrayList<Integer>();
//
//                        for(int c: MY_COLORS) colors.add(c);
//
//                        dataSet.setColors(colors);
                        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
//                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("user", "1");


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);





    }

    @Override
    protected void onResume() {
        super.onResume();
        // to check current activity in the navigation drawer
        navigationView.getMenu().getItem(0).setChecked(true);
    }


    private void GetDash() {


    }

//    private void showDialog() {
//        if (!pDialog.isShowing())
//            pDialog.show();
//    }
//
//    private void hideDialog() {
//        if (pDialog.isShowing())
//            pDialog.dismiss();
//    }
    }


