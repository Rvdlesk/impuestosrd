package lesk.wyse.ird;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by clevo on 11/17/2017.
 */

public class Tab_1_Activity extends Fragment implements View.OnClickListener  {

    EditText t;
    Button btn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.activity_tab_1, container, false);
         t = (EditText)view.findViewById(R.id.input_cedula);
         btn=(Button)view.findViewById(R.id.verifyCedula);
       btn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verifyCedula:
                String cedula = t.getText().toString().trim();
                ((TabHostActivity)getActivity()).getCedula(cedula);
                break;
            default:
                break;
        }

    }

}
