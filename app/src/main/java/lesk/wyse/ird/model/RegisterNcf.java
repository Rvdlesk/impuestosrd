package lesk.wyse.ird.model;

/**
 * Created by clevo on 2/25/2018.
 */

public class RegisterNcf {
    private String Ncf;
    private String Monto;
    private String Itbis;
    private String Fecha;
    private String Rnc;
    private String Tipo;
    private String Nombre;


    public String getNcf() {
        return Ncf;
    }

    public void setNcf(String Ncf) {
        this.Ncf = Ncf;
    }

    public String getMonto() {
        return Monto;
    }
    public void setMonto(String Monto) {
        this.Monto = Monto;

    }
    public String getItbis() {
        return Itbis;
    }
    public void setItbis(String Itbis) {
        this.Itbis = Itbis;

    }
    public String getFecha() {
        return Fecha;
    }
    public void setFecha(String Fecha) {
        this.Fecha = Fecha;

    }
    public String getRnc() {
        return Rnc;
    }
    public void setRnc(String Rnc) {
        this.Rnc = Rnc;

    }
    public String getTipo() {
        return Tipo;
    }
    public void setTipo(String Tipo) {
        this.Tipo = Tipo;

    }
    public String getNombre() {
        return Nombre;
    }
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;

    }

}
