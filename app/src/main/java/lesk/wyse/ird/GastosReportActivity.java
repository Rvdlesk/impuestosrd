package lesk.wyse.ird;

import android.app.ProgressDialog;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lesk.wyse.ird.adapter.AppConfig;
import lesk.wyse.ird.adapter.AppController;
import lesk.wyse.ird.adapter.GastoDataAdapter;
import lesk.wyse.ird.model.RegisterNcf;

public class GastosReportActivity extends BaseActivity {

    RecyclerView recyclerView;
    List<RegisterNcf> registerNcfs;
    RecyclerView.Adapter mAdapter;
    private ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
//    private GastoDataAdapter adapter;
    private static final String TAG = "Sample";
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_gastos_report, frameLayout);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        registerNcfs = new ArrayList<>();
        initViews();
    }
    @Override
    protected void onResume() {
        super.onResume();
        // to check current activity in the navigation drawer
        navigationView.getMenu().getItem(4).setChecked(true);
    }

    private void initViews(){
        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        recyclerView.setLayoutManager(layoutManager);
        getAllRegisterNcf();
    }

    private void getAllRegisterNcf() {
        // Tag used to cancel the request
        String tag_string_req = "req_rnc";

//        pDialog.setMessage("Espere");
//        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GASTO_REPORT, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {


                Log.d(TAG, "ncf: " + response.toString());
//                hideDialog();

                JSONObject json= null;


                        try {

                            RegisterNcf reg = new RegisterNcf();
                            for (int i = 0; i < response.length(); i++) {


                                JSONArray jObj = new JSONArray(response);

                                json=jObj.getJSONObject(i);
                                reg.setNcf(json.getString("Ncf"));
                                reg.setMonto(json.getString("Monto"));
                                reg.setItbis(json.getString("Itbis"));
                                reg.setFecha(json.getString("Fecha"));
                                reg.setNombre(json.getString("Nombre"));
                                reg.setRnc(json.getString("Rnc"));
                                reg.setTipo(json.getString("tipo"));



                        registerNcfs.add(reg);
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Log.d(TAG, "Json error: " + e.getMessage());
                }

                mAdapter = new GastoDataAdapter(GastosReportActivity.this, registerNcfs);
                recyclerView.setAdapter(mAdapter);
            }
            }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
//                hideDialog();
             }
            }){


        @Override
        public Map<String, String> getParams() {
            // Posting parameters to login url
            Map<String, String> params = new HashMap<String, String>();
            params.put("rns", "1");

            return params;
        }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
